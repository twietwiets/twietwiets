#!/usr/bin/env python3

# File Name: rhymewords.py
# Project Name: TwieTwiets
# Authors: Juliet Rosendaal, Mohammed Issa, Folkert Leistra, Thijmen Dam

# Description: This module returns the rhymedictionary.


def generate_rhymedictionary(database):
    """"Generates rhymedictionary from the database dpw.cd
        Key = 'rhymepattern' and value = ['word', 'word', 'word']. """

    rhymedictionary = {}

    with open(database, 'rt') as f:
        for line in f:
            word = format(line, 1)
            # We don't use lemmas  that consist of two or more words.
            if word != 'none':
                rhymepattern = indexed_pronpattern(line)
                if rhymepattern not in rhymedictionary:
                    rhymedictionary[rhymepattern] = [word]
                else:
                    rhymedictionary[rhymepattern].append(word)

    return rhymedictionary


def format(line, field):
    """ Formats: word (field = 1), pronunciation (field = 4)
        consonant/vowel = syllable (field = 5). """

    formatted_line = line.split('\\')[field]
    # We don't use lemmas  that consist of two or more words.
    if ' ' in formatted_line:
        return 'none'
    elif field == 5:
        return formatted_line[1:-2].split('][')
    elif field == 4:
        return formatted_line[1:-1].split('][')
    else:
        return formatted_line


def index_syllable(line):
    """This function looks for syllables that need to be in the rhymepattern"""

    syllables = format(line, 5)
    index = -1

    for i in range(1, len(syllables)):
        # syllables 'CV' and 'VC' can't be the last syllable:
        # the functions looks for a syllable before CV and VC.
        if syllables[-i] == 'CV' or syllables[-i] == 'VC':
            index -= 1
        else:
            break
    return index


def index_character(line):
    """ This function looks for characters from the first relevant syllable
        that need to be in the rhymepattern"""

    # Selects the first syllable from a selection of syllables
    syllable = format(line, 5)[index_syllable(line):][0]
    index = 0

    for character in syllable:
        if character == "C":
            index += 1
        else:
            break
    return index


def indexed_pronpattern(line):
    """ This function returns the relevant part of the pronunciationpattern
        of the word."""

    # Selects relevant last syllables
    pronpattern = format(line, 4)[index_syllable(line):]
    # Selects from first vowel
    pronpattern[0] = pronpattern[0][index_character(line):]
    # Returns the list as a string, syllables are seperated with a '-'
    return '-'.join(pronpattern)


def find_line(word, database):
    """ Finds the rhymepattern for the word given as input. """

    with open(database, 'rt') as f:
        for line in f:
            if word == format(line, 1):
                return line


def get_rhyme_words(word):
    """ Returns rhyme_words in a list. """

    database = '/net/corpora/CELEX/dutch/dpw/dpw.cd'
    rhymedictionary = generate_rhymedictionary(database)

    line = find_line(word, database)
    pattern = indexed_pronpattern(line)

    return rhymedictionary[pattern]
