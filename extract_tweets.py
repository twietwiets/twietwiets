#!/usr/bin/env python3

# File Name: extract_tweets.py
# Project Name: TwieTwietse
# Authors: Juliet Rosendaal, Mohammed Issa, Folkert Leistra, Thijmen Dam

# Description: This module containes functions that filter the tweet.
# The module returns the last word, tweet and a dictionary.

import re
import string
import json
import gzip
from collections import defaultdict
from valid_words import word_list
from random import randrange
from unicode_codes import UNICODE_EMOJI
from path import get_path


def get_tweet(lw_dict):
    """ Gets a random tweet and its last word.
        If the last word is not in the valid words list,
        the tweet is discarded """

    val_words = word_list()

    for key, value in lw_dict.items():
        if key in val_words:
            return key, value[randrange(len(value))]


def filter_tweets(zipped_file):
    """ Adds tweets to a list from the zipped file, except for:
        - Tweets with a 'retweeted status'
        - Tweets containing http
        - Tweets ending with @.*
        - Tweets longer than 138 characters """
    with gzip.open(zipped_file, 'rt') as file_out:
        rt = re.compile("http")
        at = re.compile(".*@\w*[.;,:*\-()\[\]/!?\'\"]*$")

        return [json.loads(line)["text"] for line in file_out
                if "retweeted_status" not in json.loads(line)
                and not rt.findall(json.loads(line)["text"])
                and not at.findall(json.loads(line)["text"])
                and not len(json.loads(line)["text"]) > 138]


def filter_last_word(tweet):
    """ Splits the tweet into a list containing words.
        The last word of the tweet is selected from the word list
        without punctuation and the function checks if the last
        word is alphanumeric """
    words = tweet.split()
    no_punct = str.maketrans('', '', string.punctuation)

    word = words[-1].translate(no_punct)

    if word.isalpha():
        return word

    else:
        return "no_word"


def last_word(tweets):
    """ Creates a dictionary containing all tweets.
        Key: last word of tweet
        Value: tweet """

    data_dict = defaultdict(list)

    for tweet in tweets:

        lw = filter_last_word(tweet)

        if not lw == "no_word":

            if lw not in data_dict:
                data_dict[lw.lower()] = [tweet]
            else:
                data_dict[lw.lower()].append(tweet)

    return data_dict


def extract(path):
    """ Extracts a Tweet and its last word from a Twitter corpus file
        and removes mentions from the tweet. The tweets are returned
        as a dictionary with key: last word, value: tweet """

    filtered_tweets = filter_tweets(path)
    lw_dict = last_word(filtered_tweets)
    lw, tweet = get_tweet(lw_dict)

    tweet = re.sub("@\w*", "", tweet)
    tweet = re.sub("  ", "", tweet)
    tweet = re.sub("&amp;", "&", tweet)

    return lw, tweet, lw_dict


def remove_emoji(tweet, rhyme_tweet):
    """ Filters possible emoji from tweets that are being displayed
        in the interface (tkinter doesn't support emoji) """
    interface_tweet = ""
    interface_rhyme_tweet = ""

    for char in tweet:
        if char not in UNICODE_EMOJI.keys():
            interface_tweet += char

    for char in rhyme_tweet:
        if char not in UNICODE_EMOJI.keys():
            interface_rhyme_tweet += char

    return interface_tweet, interface_rhyme_tweet
