#!/usr/bin/env python3

# File Name: interface.py
# Project Name: TwieTwiets
# Authors: Juliet Rosendaal, Mohammed Issa, Folkert Leistra, Thijmen Dam

# Description: This modules creates the TwieTwiets interface and
# applies their corresponding functions.


from tkinter import font, Label, Button, FLAT, GROOVE, DISABLED, NORMAL
from generate_tweets import generate_tweets
from bot import initialize_bot, publish_tweet


class tt_interface:

    def __init__(self, window):
        """ Creates the TwieTwiets interface """

        global api
        self.window = window
        window.title("Twietwiets")
        window.geometry("600x400")
        window.resizable(0, 0)

        btn_font = font.Font(family='Arial', size=20, weight='bold')
        tweet_font = font.Font(family='Arial', size=12,
                               weight='bold', slant='italic')
        tweet_label_font = font.Font(family='Arial', size=14, weight='bold')

        # Tweet Labels
        self.t1_label = Label(window, relief=FLAT, wraplength=550)
        self.t2_label = Label(window, relief=FLAT, wraplength=550)
        self.t1_label.place(x=14, y=25, height=20, width=572)
        self.t2_label.place(x=14, y=185, height=20, width=572)
        self.t1_label.configure(text="Tweet", font=tweet_label_font)
        self.t2_label.configure(text="Rijmtweet", font=tweet_label_font)

        # Tweets
        self.tweet1 = Label(window, relief=GROOVE, wraplength=550)
        self.tweet1.place(x=12, y=55, height=100, width=572)
        self.tweet1.configure(text="", font=tweet_font)

        self.tweet2 = Label(window, relief=GROOVE, wraplength=550)
        self.tweet2.place(x=12, y=215, height=100, width=572)
        self.tweet2.configure(text="", font=tweet_font)

        # Buttons
        generate = Button(window)
        generate.place(x=11, y=345, relwidth=0.3)
        self.publish = Button(window)
        self.publish.place(x=208, y=345, relwidth=0.3)
        exit_button = Button(window)
        exit_button.place(x=405, y=345, relwidth=0.3)

        # Button Configuration
        generate.configure(text="Generate",
                           font=btn_font, command=self.gen_tweets)
        self.publish.configure(text="Publish",
                               font=btn_font, command=self.publish_rhyme,
                               state=DISABLED)
        exit_button.configure(text="Exit", font=btn_font, command=window.quit)

        # Initializing Bot
        api = initialize_bot()

    def gen_tweets(self):
        """ Generates the tweet and rhymetweet when pressing
            the 'generate' button. """

        global tweet
        global rhymetweet
        self.publish.configure(state=DISABLED)
        self.tweet1.configure(text="{0}".format("Finding tweets..."))
        self.tweet2.configure(text="{0}".format(""))
        self.window.update()
        tweet, rhymetweet = generate_tweets()
        self.publish.configure(state=NORMAL)
        self.window.update()
        try:
            self.tweet1.configure(text="{0}".format(tweet))
            self.tweet2.configure(text="{0}".format(rhymetweet))
        except:
            self.tweet1.configure(text="Error: Tweet bevat tekens "
                                       "die de interface niet kan weergeven.")

    def publish_rhyme(self):
        """ Publishes the tweet and rhymetweet to the twitter account
            @TheTwieTwiets """

        publish_tweet(api, tweet, rhymetweet)
        self.publish.configure(state=DISABLED)
        self.window.update()
