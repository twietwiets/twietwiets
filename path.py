#!/usr/bin/env python3

# File Name: path.py
# Project Name: TwieTwiets
# Authors: Juliet Rosendaal, Mohammed Issa, Folkert Leistra, Thijmen Dam

# Description: This module generates a random absolute path to a corpus file
# located at the RUG servers.


from os import walk
from random import randrange


def get_path():

    year = randrange(2014, 2019)

    months = []
    files = []

    for (dirpath, dirnames, filenames) in walk(
            "/net/corpora/twitter2/Tweets/{0}" .format(year)):
        months.extend(dirnames)
        break

    month = months[randrange(len(months))]

    for (dirpath, dirnames, filenames) in walk(
            "/net/corpora/twitter2/Tweets/{0}/{1}" .format(year, month)):
        files.extend(filenames)
        break

    file = files[randrange(len(files))]

    return ("/net/corpora/twitter2/Tweets/{0}/{1}/{2}"
            .format(year, month, file))
