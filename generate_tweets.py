#!/usr/bin/env python3

# File Name: generate_tweets.py
# Project Name: TwieTwiets
# Authors: Juliet Rosendaal, Mohammed Issa, Folkert Leistra, Thijmen Dam

# Description: This is the core functionality of TwieTwiets.


from extract_tweets import *
from rhymewords import get_rhyme_words


def generate_tweets():
    """ Core functionality of TwieTwiets. """

    # Gets random valid tweet from path and its corresponding last word
    print("Finding tweet...")

    lw, tweet, lw_dict = extract(get_path())

    while not len(tweet) <= 100:
        lw, tweet, lw_dict = extract(get_path())

    print("Tweet: {0} \nLast word: {1}\nFinding rhyme tweet..."
          .format(tweet, lw))

    rhyme_words = get_rhyme_words(lw)

    nf_counter = 0

    while True:

        found = False

        for word in rhyme_words:

            if word in lw_dict and word != lw:
                rhyme_tweets = lw_dict[word]
                rhyme_tweet = rhyme_tweets[randrange(len(rhyme_tweets))]

                rhyme_tweet = re.sub('@\w*', '', rhyme_tweet)
                rhyme_tweet = re.sub('  ', '', rhyme_tweet)

                rhymeword_list = rhyme_tweet.split()
                no_punct = str.maketrans('', '', string.punctuation)
                lw_rhymetweet = rhymeword_list[-1].translate(no_punct)

                if (rhyme_tweet != tweet
                        and len(tweet) - 20 < len(rhyme_tweet)
                        < len(tweet) + 20
                        and lw not in lw_rhymetweet
                        and len(lw_rhymetweet) > 2):
                    print("Rhyme tweet: {0}".format(rhyme_tweet))
                    print("Last rhyme word: {0}\nDone\n".format(lw_rhymetweet))
                    found = True
                    break

        if not found:

            if nf_counter < 3:
                nf_counter += 1
                path = get_path()
                new_tweets = filter_tweets(path)
                lw_dict = last_word(new_tweets)
                print("Rhyme word not found, searching in new corpus file.")

            else:
                print("Error: Rhyme word not found timeout. "
                      "Generating new tweets.\n")
                return generate_tweets()
        else:

            tweet, rhyme_tweet = remove_emoji(tweet, rhyme_tweet)
            return tweet, rhyme_tweet
