#!/usr/bin/env python3

# File Name: twietwiets.py
# Project Name: TwieTwiets
# Authors: Juliet Rosendaal, Mohammed Issa, Folkert Leistra, Thijmen Dam

# Description: This is the final TwieTwiets program.
# Execute this file to start the program.


from tkinter import *
from interface import tt_interface


def main():

    window = Tk()
    tt_interface(window)
    window.mainloop()


if __name__ == "__main__":

    main()
