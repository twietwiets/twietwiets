#!/usr/bin/env python3

# File Name: twietwiets_stream.py
# Project Name: TwieTwiets
# Authors: Folkert Leistra, Thijmen Dam

# Description: This module contains the core Twitter bot stream functionality.


from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy import API
from tweepy import Cursor
from extract_tweets import *
from rhymewords import get_rhyme_words
from valid_words import word_list


consumer_key = "siGP3knPk4b8lAMGyhgxmefEs"
consumer_secret = "FaFNqaY1FrpvSrLMzBhTIbtUON3ySLbTBm0aYmfhobMORm5dKn"
access_token = "971798735536738306-xTIlL9QKeyFp5S7ncUlpEpOVrRa9FMM"
access_secret = "SDw9QaVspItPUhY1rBPV0q124qqiwfaOtSvfWGqWY2vqg"


class StdOutListener(StreamListener):

    def on_data(self, data):
        tweet_handler(json.loads(data))
        return False

    def on_error(self, status):
        print(status)


def delete_tweets():
    """ When active, deletes all (!)
        tweets of @TheTwieTwiets """

    timeline = Cursor(api.user_timeline).items()

    for tweet in timeline:
        api.destroy_status(tweet.id)


def mention_check(tweet):
    """ Checks if the @TheTwieTwiets is mentioned
        properly """

    ttt1 = re.compile("^@TheTwieTwiets.*")
    ttt2 = re.compile("^@thetwietwiets.*")

    if ttt1.findall(tweet) or ttt2.findall(tweet):
        return True
    else:
        return False


def filter_check(tweet):
    """ Checks if the tweet contains a URL,
        if the tweets end with a mention
        and if the tweet is too long"""

    rt = re.compile("http")
    at = re.compile(".*@\w*[.;,:*\-()\[\]/!?\'\"]*$")

    if rt.findall(tweet) or at.findall(tweet):
        return False

    else:
        return True


def tweet_handler(tweet_data):
    """ Core functionality of the
        streaming bot """

    tweet = tweet_data["text"]
    id = tweet_data["id"]
    username = tweet_data["user"]["screen_name"]

    print("\nMention ontvangen:")
    print("Username: {0}" .format(username))
    print("Tweet: {0}" .format(tweet))

    if mention_check(tweet):
        if filter_check(tweet):
            if filter_last_word(tweet) != "no_word":
                    lw = filter_last_word(tweet)
                    if lw in word_list():
                        rhymetweet = get_tweets_bot(tweet, lw, username)
                        if rhymetweet != "Niks gevonden!":
                            api.update_status("@{0} {1}\n#TheTwieTwiets"
                                              .format(username, rhymetweet), id)
                        else:
                            api.update_status("@{0} Helaas! "
                                              "Ik kan hier niks op bedenken..."
                                              .format(username), id)
                    else:
                        api.update_status("@{0} Helaas! Dat laatste woord "
                                          "ken ik niet."
                                          .format(username), id)
                        print("Error: Laatste woord "
                              "staat niet in de rijmdatabase.")
            else:
                api.update_status("@{0} Verkeerd aangesproken. "
                                  "Laatste token is geen woord!"
                                  .format(username), id)
                print("Error: Laatste token is geen woord")
        else:
            api.update_status("@{0} Help! Deze tweet begrijp ik niet. Bevat "
                              "hij soms een URL? Een mention aan het einde? "
                              "Of is hij te lang? Lastig..."
                              .format(username), id)
            print("Error: URL of Mention aan het einde / Tweet is te lang.")
    else:
        api.update_status("@{0} Verkeerd aangesproken. "
                          "Mention mij vooraan in je tweet!"
                          .format(username), id)
        print("Error: Mention klopt niet")


def get_tweets_bot(tweet, lw, username):
    """ Core functionality of the rhyme-answer Twitter bot """

    print("Last word: {0}" .format(lw))
    print("Searching for rhyming tweet")

    rhyme_words = get_rhyme_words(lw)
    nf_counter = 0

    filtered_tweets = filter_tweets(get_path())
    lw_dict = last_word(filtered_tweets)

    while True:

        found = False

        for word in rhyme_words:

            if word in lw_dict and word != lw:
                rhyme_tweets = lw_dict[word]
                rhyme_tweet = rhyme_tweets[randrange(len(rhyme_tweets))]

                rhyme_tweet = re.sub('@\w*', '', rhyme_tweet)
                rhyme_tweet = re.sub('  ', '', rhyme_tweet)

                rhymeword_list = rhyme_tweet.split()
                no_punct = str.maketrans('', '', string.punctuation)
                lw_rhymetweet = rhymeword_list[-1].translate(no_punct)

                if (rhyme_tweet != tweet
                        and len(tweet) - 30 < len(rhyme_tweet)
                        < len(tweet) + 30
                        and lw not in lw_rhymetweet
                        and len(lw_rhymetweet) > 2):
                    print("Rhyme tweet: {0}\nDone".format(rhyme_tweet))
                    found = True
                    break

        if not found:

            if nf_counter < 10:
                nf_counter += 1
                path = get_path()
                new_tweets = filter_tweets(path)
                lw_dict = last_word(new_tweets)
                print("Rhyme word not found, searching in new corpus file.")

            else:
                print("Helaas! Hier kan ik niks op rijmen.")
                return "Niks gevonden!"

        else:

            return rhyme_tweet


if __name__ == "__main__":

    listener = StdOutListener()
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_secret)
    api = API(auth)
    stream = Stream(auth, listener)

    # Uncomment the following line to delete all(!) current tweets.
    # delete_tweets()

    while True:
        stream.filter(track=["@TheTwieTwiets", "@thetwietwiets"])
