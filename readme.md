# Twietwiets

Twietwiets schrijft tweet-rijmpjes. Het programma bestaat uit twee onderdelen:  

* 'twietwiets.py' - Dit onderdeel pakt een willekeurige Nederlandse tweet en zoekt daarbij automatisch een Nederlandse tweet die daarop rijmt. Ook is er de mogelijkheid om deze rijmende tweets te publiceren op de Twitter-account @TheTwieTwiets. Dit alles is iut te voeren met behulp van een grafische interface.
* 'twietwiets_stream.py' - Dit onderdeel maakt het mogelijk om de Twitter-account @TheTwieTwiets te mentionen op Twitter, waarna @TheTwieTwiets automatisch zal antwoorden op de gebruiker van de desbetreffende tweet met een rijmende willekeurige Nederlandse tweet.

De onderdelen dienen niet op hetzelfde moment te worden uitgevoerd.


## LEZEN VOOR GEBRUIK

Twietwiets is alleen te gebruiken op een LWP machine van de Rijksuniversiteit Groningen.  

De Tweepy en tkinter modules dienen te worden geinstalleerd om dit programma correct te kunnen uitvoeren.

Dit kan via de volgende commando's:
`pip3 install tweepy, pip3 install tkinter`

Werkt dit niet in verband met gebruikersrechten, gebruik dan de volgende commando's:
`pip3 install --user tweepy, pip3 install --user tkinter`

De emoji module hoeft niet geinstalleerd te worden - deze is reeds bijgevoegd in de repository als `unicode_codes.py`.


## Gebruik

'twietwiets.py' is aan te roepen met 'python3 twietwiets.py' via de commandline. Dit zal een interface openen waarin de gebruiker tweets kan genereren en eventueel kan publiceren op het Twitter-account @TheTwieTwiets.  

'twietwiets_stream.py' is aan te roepen met 'python3 twietwiets_stream.py' via de commandline. Dit start een stream die alle tweets binnenhaalt die @TheTwieTwiets mentionen. Wanneer de mention-tweet voldoet aan de door ons gestelde eisen, zal @TheTwieTwiets de desbtereffende mention-tweet beantwoorden met een willekeurige rijmende Nederlandse tweet.  


### Index

/twietwiets  

bot.py  
extract_tweets.py  
generate_tweets.py  
interface.py  
path.py  
readme.md  
rhymewords.py  
twietwiets_stream.py  
twietwiets.py  
unicode_codes.py  
valid_words.py  


### Modulestructuur

De structuur van de belangrijkste modules is als volgt:

* twietwiets.py > `get_path()` : toegang tot tweets-database.
* twietwiets.py > `extract(path)` : aanleg dictionairy `lw-dict` op basis van tweets-database. Key = laatste woord van de tweet. Value = lijst met tweets met hetzelfde woord als laatste woord.
    * extract_tweets.py > `word_list()` : controleert of de toe te voegen Key van `lw-dict` beschikbaar is in uitspraak-database. Als niet, dan worden Key en Value niet toegevoegd.
* twietwiets.py > `get_rhyme_words(lw)` : laatste woord van input-tweet wordt doorgegeven aan rijmalgoritme. Rijmalgoritme geeft lijst van rijmwoorden terug.
    * rhymewords.py > `generate_rijmwoordenboek(database)` : aanleg dictionary `rijmwoordenboek` op basis van uitspraak-database. Key = gecodeerde uitspraak van het relevante deel van het rijmwoord. Value = lijst met alle woorden met dezelfde relevante uitspraak.
        * `generate_rijmwoordenboek(database)` > per woord wordt het relevante rijmdeel als volgt geconstateerd:
            * `index-lettergreep(line)` : Het woord wordt van achteren naar voren gelezen. Als de uitspraakcodering van een lettergreep 'CV' of 'VC' is, en daarmee lettergreep die meestal niet benadrukt wordt, wordt gekeken naar de volgende (vorige) lettergreep.
            * `index-letter(line)` : Uit de eerste lettergreep van de geselecteerde lettergrepen uit `index-lettergreep(line)` worden de letters geselecteerd vanaf de eerste klinker.
            * `indexed_pronpattern(line)` : Past de indices van `index-lettergreep(line)` en `index-letter(line)` toe op het rijmwoord.
* twietwiets.py zoekt in `lw-dict` met als Key rijmwoord. 

### Vereiste databases

* twitter-database
    * bestandsnaam: `(jaar)(maand)(datum)_(uur).gzip.out`
    * geformatteerd als gzip.out
    * tweets gefromatteerd in json  

* uitspraak-database
    * bestandsnaam: `dpw.cd`
    * data gescheiden door: `\`
    * indeling data: `id-nummer-woord\woord\id-nummer-lemma\uitspraak-notatie1\uitspraak-notatie2\klinker-medeklinker-patroon`


### Interface

'twietwiets.py' - 'Generate' om tweets te genereren. 'Publish' om de tweet + rijmtweet te publiceren op Twitter. 'Exit' om af te sluiten. De terminal wordt gebruikt als debug-console.
'twietwiets_stream.py' - Gebruikt de terminal als interface waarop informatie wordt weergegeven wanneer een tweet die @TheTwieTwiets mentiont wordt binnengehaald.


## Gebruikte Software

* [Sublime Text](https://www.sublimetext.com/) - DevTool
* [PyCharm](https://www.jetbrains.com/pycharm/) - DevTool

## Versie

Versie 1.0 (10/04/2018)
Zie [BitBucket repository]https://bitbucket.org/twietwiets/twietwiets).

## Auteurs

* **Thijmen Dam**
* **Mohammad Issa**
* **Folkert Leistra**
* **Juliet van Rosendaal**

## Logboek
wanneer, wie, wat  

7/3/2018: Folkert &Mohammad & Thijmen & Juliet: Werkcollege gitrepository opzetten.  
12/3/2018: Folkert & Mohammad & Thijmen & Juliet: Automatisch zoeken in tweets in database.  
12/3/2018: Folkert & Thijmen: Extract tweets via regex.  
12/3/2018: Mohammad: Extract tweets via .split.  
12/3/2018: Juliet: Extract tweets via json.  
12/3/2018: Folkert & Thijmen: Combineren json extractor met unzipper.  
12/3/2018: Mohammad: Werkt verder aan dict genereren.  
12/3/2018: Thijmen: Twitter bot concept (automatisch plaatsen van een tweet).  
14/3/2018: Thijmen & Folkert: Regex & json filter url en RT's.  
14/3/2018: Juliet: Codering emoji's & structuur json.  
19/3/2018: Juliet & Mohammad: Ontwikkelen dict met key = rijmwoord value = tweet/lijst van tweets.  
23/3/2018: Mohammad: Dict opschonen emoji en punctuation.  
23/3/2018: Juliet: Begin ontwikkeling rijmalgoritme.  
29/03/2018: Folkert & Thijmen: Code opgeschonen en opgedeeld in functies tweet filter verbeterd en last word filter aangemaakt.  
29/03/2018: Juliet: Verbetert en breidt uit rijmalgoritme.  
2/04/2018: Thijmen: Module aangemaakt voor het automatisch genereren van een filepath begonnen aan het overkoepelend programma bot concept nu functioneel incl. replies.  
2/04/2018: Mohammad: Beginnen aan interface.  
2/04/2018: Juliet: Herschrijven rijmalgoritme.  
4/04/2018: Thijmen & Folkert: Verder met het werkend maken van het overkoepelende programma.  
4/04/2018: Juliet: Afmaken rijmalgoritme.  
9/04/2018: Juliet: Schrijven en opmaak readme.md.  
9/04/2018: Folkert: Vertalen,straktrekken definities en pep8, extra filters toegepast.  
9/04/2018: Thijmenn & Mohammad: interface verbeterd.  
9/04/2018: Thijmen: Twitter bot verbeterd. Groot aantal buxfixes. Functies toegevoegd dat het programma altijd doorgaat, ook als bepaalde items (rijmwoord, geschikte tweet etc.) niet worden gevonden.  
10/04/2018: Thijmen & Folkert: Rijmalgoritme verbeterd. Console meldingen correct toegevoegd. Interface volledig werkend incl. publish naar Twitter.  
10/04/2018: Thijmen & Folkert: Alles gecontroleerd op pycodestyle + comments/docstrings toegevoegd.  
10/04/2018: Thijmen & Folkert: Twitter stream werkend gemaakt: mogelijkheid om de bot te mentionen in een tweet, de bot stuurt hier als antwoord een rijmende tweet op terug.  
10/04/2018: Thijmen & Folkert: Uiteindelijke versie README.md gepubliceerd.
