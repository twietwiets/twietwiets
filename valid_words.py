#!/usr/bin/env python3

# File Name: valid_words.py
# Project Name: TwieTwiets
# Authors: Juliet Rosendaal, Mohammed Issa, Folkert Leistra, Thijmen Dam

# Description: this module returns a set containg all words in dpw.cd database.


def word_list():
    """ Adds all words from dpw.cd to a set"""
    words = set()
    with open("/net/corpora/CELEX/dutch/dpw/dpw.cd", 'rt') as f:
        for line in f:
            words.add(line.split('\\')[1])

    return words
